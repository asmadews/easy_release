# -*- coding: utf-8 -*


def parse_console_scripts_configuration(config_file):
    """Take data_files configuration
    and prepare data files structure for setup.py"""
    console_scripts_items = config_file.items('console_scripts')

    print("Processing console_scripts section")
    # return list of lines like: script_name=module:target
    # where script_name taken from config key and target from value
    # target should be in https://python-packaging.readthedocs.io/en/latest/
    # command-line-scripts.html#the-console-scripts-entry-point format
    return [
        '%s=%s' % (item, target_path)
        for item, target_path
        in console_scripts_items
    ]
