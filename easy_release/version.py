#!/usr/bin/env python
#
"""Package easy_release: Python project release bumper"""

version = "0.0.9"
version_info = (0, 0, 9, 0)
