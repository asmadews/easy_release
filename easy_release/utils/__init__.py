# -*- coding: utf-8 -*-
from functools import partial


def change_keyname_dashes_to_underscore(dic):
    return dict([(k.replace('-', '_'), dic[k]) for k in dic])


def pop_item(dic, key):
    """ Returns tuple: either (key, item) if key exists, or (key, None)."""
    return key, dic.pop(key, None)


def normalize(options):
    """ Return correct kwargs for setup() from provided options-dict.

    1. make copy of options-dict and use the copy
    2. change all dashes to underscores in keynames
    3. go over keys which needs attention and make them setup-compatible
    4. return n(ormalized)options-dict
    """
    noptions = change_keyname_dashes_to_underscore(options.copy())
    pop = partial(pop_item, noptions)

    (key, value) = pop('classifier')
    if value and isinstance(value, str):
        noptions[key] = "\n\t".join(value.splitlines())
        # there is no else; either we know how to handle it, or leave it
    return noptions
