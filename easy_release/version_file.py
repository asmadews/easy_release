# -*- coding: utf-8 -*-
""" Define version utility class """
import importlib
import logging
import os
import sys
from six.moves.configparser import ConfigParser

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

version_file_template = '''#!/usr/bin/env python
#
"""Package {package_name}: {human_readable_package_name}"""

version = "{version}"
version_info = ({version_info})
'''


UPDATE_MAJOR = 'major'
UPDATE_MINOR = 'minor'
UPDATE_DEVEL = 'devel'

UPDATE_TYPES = [UPDATE_MAJOR, UPDATE_MINOR, UPDATE_DEVEL]


class VersionFile(object):
    """Version file model, providing methods to load current version,
    calculate next version number increasing major, minor or developer number,
    increasing version with selected model and writing changes back
    """

    def __init__(self, current_path, config_file):
        super(VersionFile, self).__init__()

        assert isinstance(config_file, ConfigParser)
        self.config_file = config_file
        self.current_path = current_path
        self.has_changes = False

        self._version_string_loaded = False
        self._update_type = None

        self._version_string = None
        self._version_info = None
        self._next_version_string = None
        self._next_version_info = None

        self.load()

    def get_version_module_name(self):
        """Take version containing module name"""
        package = self.config_file.get('metadata', 'name')
        version_module = self.config_file.get(
            'setup', 'version_module', fallback='__init__'
        )
        if not version_module.startswith(package):
            version_module = package + '.' + version_module

        if version_module.endswith('.py'):
            version_module = version_module[:-3]

        if version_module.endswith('.__init__'):
            version_module = version_module[:-9]

        return version_module

    def get_version_module_path(self):
        """Take version containing module absolute file path"""
        version_path = self.get_version_module_name()
        if '.' in version_path:
            version_path = version_path.replace('.', os.sep)

        package = self.config_file.get('metadata', 'name')
        if not version_path.startswith(package):
            version_path = os.path.join(package, version_path)

        if version_path.endswith(package):
            version_path = os.path.join(version_path, '__init__')

        if not version_path.endswith('.py'):
            version_path += '.py'

        version_path = os.path.realpath(version_path)
        return version_path

    def _make_next_version_info(self):
        """Set next version string & next version_info basing on
        update_type to be set before with set_update_type"""
        if self._update_type is None:
            raise NotImplementedError(
                "Set update_type=[%s] first".format(
                    '|'.join(UPDATE_TYPES)
                )
            )
        if self._update_type == UPDATE_MAJOR:
            self._next_version_info = [
                self._version_info[0] + 1,
                0,
                0,
                0
            ]
        elif self._update_type == UPDATE_MINOR:
            self._next_version_info = [
                self._version_info[0],
                self._version_info[1] + 1,
                0,
                0
            ]
        elif self._update_type == UPDATE_DEVEL:
            self._next_version_info = [
                self._version_info[0],
                self._version_info[1],
                self._version_info[2] + 1,
                0
            ]
        self._next_version_string = '.'.join([
            str(x) for x in self._next_version_info[:-1]
        ])

    def get_update_type(self):
        """Get currently set update type"""
        return self._update_type

    def set_update_type(self, update_type):
        """Set update type for current version.
        Additionally recalculates internals:
        - _next_version_string
        - _next_version_info
        """
        if update_type not in UPDATE_TYPES:
            raise RuntimeError(
                "Unknown update style, "
                "expected one of %s".format(
                    ', '.join(UPDATE_TYPES)
                )
            )
        self._update_type = update_type
        self._make_next_version_info()
        logger.debug(
            "Current version %s, "
            "update %s "
            "will produce %s".format(
                self._version_string,
                self._update_type,
                self._next_version_string
            )
        )

    update_type = property(get_update_type, set_update_type)

    def load(self):
        """Load version file & extract version string & version info.
        Additionally constructs next version string & next version info tuple,
        assuming developer update type increasing 3-rd part of version

        Possible update types for version X.Y.Z:

        - UPDATE_MAJOR: X+=1
        - UPDATE_MINOR: Y+=1
        - UPDATE_DEVEL: Z+=1
        """
        if not self._version_string_loaded:
            logger.info("Loading version from version file")
            version_module = self.get_version_module_name()
            logger.debug("Version module is %s", version_module)

            # set current path as sources root and preferred path for imports
            sys.path.insert(0, self.current_path)

            logger.debug("Loading %s", version_module)
            # import version module
            module_obj = importlib.import_module(version_module)

            # prevent version number caching by module reload
            importlib.reload(module_obj)

            # unset current path as sources root and preferred path for imports
            sys.path.pop(0)

            version_attribute = 'version'
            version_info_attribute = 'version_info'

            # Load version string
            logger.debug(
                "Take current version string as %s.%s",
                version_module, version_attribute
            )
            self._version_string = getattr(
                module_obj,
                version_attribute,
                '0.0.0'
            )
            logger.debug("Current version =%s", self._version_string)

            # Load version info
            logger.debug(
                "Take current version string from %s.%s",
                version_module, version_info_attribute
            )
            self._version_info = getattr(
                module_obj,
                'version_info',
                (0, 0, 0, 0)
            )
            logger.debug("Current version info %s", self._version_info)

            self._version_string_loaded = True
            self.has_changes = False

    def increase(self):
        """Increase version internally"""
        if self.update_type is None:
            raise RuntimeError("Set update type first")

        self._version_string = self._next_version_string
        self._version_info = self._next_version_info

        self._make_next_version_info()
        self.has_changes = True

    def write(self):
        """Write version number into version file"""
        if not self.has_changes:
            logger.error("No changes made, will not actually write new file")

        logger.info("Write new version into version file")
        package = self.config_file.get('metadata', 'name')
        readable = self.config_file.get(
            'metadata', 'description', fallback=package
        )

        version = self._version_string
        logger.debug("Got version number %s from metadata", version)

        version_info_string = ', '.join([str(x) for x in self._version_info])

        version_file_path = self.get_version_module_path()

        init_code = version_file_template.format(
            package_name=package,
            human_readable_package_name=readable,
            version=version,
            version_info=version_info_string
        )
        logger.debug("Write version file %s", version_file_path)
        with open(version_file_path, 'w', encoding='utf-8') as setup_file:
            setup_file.write(init_code)

        self.load()

    def get_current_version(self):
        """Get current version string set in version module"""
        return self._version_string

    current_version = property(get_current_version)
