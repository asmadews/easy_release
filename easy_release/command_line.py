#!/usr/bin/env python
"""Command line callable functions
"""

import logging
# -*- coding: utf-8 -*-
import optparse
import os
import re
import sys

from six.moves.configparser import ConfigParser

from easy_release.changes_txt import ChangesTxtFile
from easy_release.setup_py import SetupPy
from easy_release.version_file import UPDATE_DEVEL
from easy_release.version_file import UPDATE_MAJOR
from easy_release.version_file import UPDATE_MINOR
from easy_release.version_file import UPDATE_TYPES
from easy_release.version_file import VersionFile

logging.basicConfig()

href_re = re.compile(r'\((https?://.+)\)')

CONFIG_FILE_NAME = 'easy_release.cfg'
CURRENT_PATH = os.path.realpath(os.curdir)
RELEASE_CONFIG = os.path.join(CURRENT_PATH, CONFIG_FILE_NAME)

if not os.path.exists(RELEASE_CONFIG):
    sys.stdout.write('No %s found'.format(CONFIG_FILE_NAME))
    exit(64)


def parse_option(args):
    """Parse command line options """
    usage = """%(name)s make new version for Python project

    Usage: %(name)s [options]""".format(name=args[0])
    parser = optparse.OptionParser(usage=usage)
    parser.add_option(
        '-i',
        '--increment',
        action='store',
        choices=UPDATE_TYPES,
        default=UPDATE_DEVEL,
        help='Possible choices: %s'.format(
            ",".join(UPDATE_TYPES)
        )
    )

    options, args = parser.parse_args(args)
    if options.increment not in UPDATE_TYPES:

        parser.error('Unknown increase type %s'.format(options.increment))

    return options, args


def increase_version(update_type):
    """Make new version, using specified update_type"""
    logger = logging.getLogger('increase_minor')
    logger.setLevel(logging.DEBUG)

    config_file = ConfigParser()
    config_file.read(RELEASE_CONFIG)
    logger.info("Using config file %s" % RELEASE_CONFIG)

    # update version file
    version_file = VersionFile(CURRENT_PATH, config_file)
    logger.info(
        "Using version file %s" % version_file.get_version_module_path()
    )
    version_file.update_type = update_type
    version_file.increase()
    version_file.write()

    # search commit messages to place in changes.txt
    changes_txt = ChangesTxtFile(CURRENT_PATH, config_file)
    changes_txt.write(version_file.current_version)

    setup_py = SetupPy(CURRENT_PATH)
    setup_py.set_config(config_file)
    setup_py.find_options()
    setup_py.write()

    if version_file.has_changes and changes_txt.using_git:
        changes_txt.commit(version_file.current_version)


def increase_devel():
    """Increase devel version number"""
    increase_version(UPDATE_DEVEL)


def increase_minor():
    """Increase minor version number"""
    increase_version(UPDATE_MINOR)


def increase_major():
    """Increase major version number"""
    increase_version(UPDATE_MAJOR)


def increase_with_commandline():
    """Process commandline options & process specified increase"""
    options, args = parse_option(sys.argv)
    increase_version(options.increment)


if __name__ == '__main__':
    increase_with_commandline()
