EasyRelease
======================================

.. toctree::
   :glob:
   :maxdepth: 1

   install/index
   configure/index

Индексы и таблицы
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
