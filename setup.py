#!/usr/bin/env python
# -*- coding: utf-8 -*-


from setuptools import setup

# noinspection PyPackageRequirements
setup(
    name="easy_release",
    description="Python project release bumper",
    author="Marin Aleksey",
    author_email="asmadews@gmail.com",
    include_package_data=True,
    license="None",
    platforms="POSIX",
    long_description="Long description",
    version="0.0.9",
    packages=[
        "easy_release",
        "easy_release.changes"
    ],
    data_files=[
    ],
    scripts=[
    ],
    entry_points={
        "console_scripts": [
            "easy_release=easy_release.command_line:increase_with_commandline"
        ]
    },
    install_requires=['six']
)

classifiers = [
    'Development Status :: 4 - Beta',
    'Intended Audience :: Developers',
    'Topic :: Software Development :: Build Tools',
    'License :: OSI Approved :: MIT License',
    'Environment :: No Input/Output (Daemon)',
    'Intended Audience :: Financial and Insurance Industry',
    'License :: Other/Proprietary License',
    'Natural Language :: Russian',
    'Operating System :: POSIX :: Linux',
    'Programming Language :: Python :: 3.6',
    'Topic :: Office/Business :: Financial :: Accounting'
]
