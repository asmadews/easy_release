# -*- coding: utf-8 -*-

from easy_release.changes import ChangesProcessor
from easy_release.history_formatter import format_change_line
from easy_release.history_formatter import format_version_line


class ChangesFileParser(ChangesProcessor):

    def __init__(self, version_re, commit_re, default_change='Refactoring'):
        super().__init__(version_re, commit_re, default_change)

        self.version_line_formatter = format_version_line
        self.change_line_formatter = format_change_line

        self.last_line_is_version = False
        self.last_line_is_change = False

    def format_version_line(self, line):
        return self.version_line_formatter(line)

    def format_change_line(self, line):
        return self.change_line_formatter(line)

    def add_version_line(self, line):
        if self.last_line_is_version:
            self.add_change_line(self._default_commit_message)

        self.version_lines.append(self.format_version_line(line))
        self.last_line_is_version = True
        self.last_line_is_change = False

    def add_change_line(self, line):
        self.version_lines.append(self.format_change_line(line))
        self.last_line_is_change = True
        self.last_line_is_version = False

    def get_changes_from_changes(self, new_version_number):
        self.add_version_line(new_version_number)
        self.add_change_line(self._default_commit_message)
        return self.version_lines
