# -*- coding: utf-8 -*-
from easy_release.utils import normalize


def metadata_from_setupcfg(config_file):
    """
    Read the [metadata] section of setup.cfg and return it as a dict.

    This is the main-function. All other functions in this file can be viewed
    as helpers.
    """
    section_name = 'metadata'
    options = dict(config_file.items(section_name))
    return normalize(options)
