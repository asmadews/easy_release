# -*- coding: utf-8 -*-
"""Define Changes.txt wrapper class"""

import os

from six.moves.configparser import ConfigParser

from easy_release.git_history import GitDisabled
from easy_release.git_history import GitHistoryProcessor

default_max_versions = 10


class ChangesTxtFile(object):
    """Wrap Changes.txt methods"""

    def __init__(self, current_path, config_file):
        super(ChangesTxtFile, self).__init__()
        assert isinstance(config_file, ConfigParser)
        self.config_file = config_file
        self.current_path = current_path

    @property
    def using_git(self):
        """Detect if using git"""
        return GitHistoryProcessor(self.config_file).usable

    def write(self):
        """Write changes made upto now"""
        changes_file_path = os.path.join(
            self.current_path,
            'CHANGES.txt'
        )

        if not self.using_git:
            raise GitDisabled("Cant gather changes without git")

        git_history_parser = GitHistoryProcessor(self.config_file)
        change_lines = git_history_parser.get_changes_from_git_history()

        changes_txt = self.config_file.get(
            'changes', 'header', fallback='Changes'
        )
        changes_txt += '\n\n' + '\n'.join(change_lines)

        print('Updating CHANGES.txt')
        with open(changes_file_path, 'w', encoding='utf-8') as changes_file:
            changes_file.write(changes_txt)

    def commit(self, new_version):
        """Commit version file via git"""
        if not self.using_git:
            raise GitDisabled("Cant gather changes without git")

        git_history_parser = GitHistoryProcessor(self.config_file)
        git_history_parser.commit_version(new_version)
