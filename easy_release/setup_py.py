# -*- coding: utf-8 -*-
"""Utilities to deal with setup.py"""

import datetime
import os

from easy_release.console_scripts import parse_console_scripts_configuration
from easy_release.data_files import parse_datafiles_configuration
from easy_release.pformat import pformat
from easy_release.utils.scripts import get_scripts
from easy_release.version_file import VersionFile

setup_py_template = u"""#!/usr/bin/env python
# -*- coding: utf-8 -*-
%(file_description)s

from setuptools import setup

# noinspection PyPackageRequirements
setup(%(setup_options_formatted)s)

classifiers = [%(classifiers)s\n]
"""


class SetupPy(object):
    """Project setup.py wrapper class"""

    def __init__(self, path):
        super(SetupPy, self).__init__()

        self.version = None
        if not path.endswith('setup.py'):
            path = os.path.join(path, 'setup.py')
        self.path = path

        self.packages = []
        self.scripts = []
        self.config_file = None
        self.classifiers = []
        self.data_files = []
        self.console_scripts = []

    def set_packages(self, packages):
        """set_packages"""
        self.packages = packages

    def set_scripts(self, scripts):
        """set_scripts"""
        self.scripts = scripts

    def set_console_scripts(self, console_scripts):
        """set_console_scripts"""
        self.console_scripts = console_scripts

    def set_config(self, config):
        """set_config"""
        self.config_file = config

    def set_classifiers(self, classifiers):
        """set_classifiers"""
        self.classifiers = classifiers

    def set_datafiles(self, datafiles):
        """set_datafiles"""
        self.data_files = datafiles

    @staticmethod
    def find_packages():
        """find_packages"""
        from setuptools import find_packages

        exclude_packages = ('tests.*',)
        packages = find_packages(exclude=exclude_packages)
        packages.sort()
        packages = [
            x
            for x in packages
            if not any([x.startswith(y) for y in exclude_packages])
        ]
        return packages

    def find_datafiles(self):
        """find_datafiles"""
        current_path = os.path.dirname(self.path)
        return parse_datafiles_configuration(current_path, self.config_file)

    def find_scripts(self):
        """find_scripts"""
        current_path = os.path.dirname(self.path)
        return get_scripts(current_path, self.config_file)

    def find_version(self):
        """Find version string currently set in version file"""
        version_file = VersionFile(self.path, self.config_file)
        return version_file.current_version

    def set_version(self, new_version):
        """Prepare new version string to write into setup.py"""
        self.version = new_version

    def find_console_scripts(self):
        """find_console_scripts"""
        return parse_console_scripts_configuration(self.config_file)

    def find_options(self):
        """find_options"""
        self.set_scripts(self.find_scripts())
        self.set_packages(self.find_packages())
        self.set_datafiles(self.find_datafiles())
        self.set_classifiers(self.find_classifiers())
        self.set_console_scripts(self.find_console_scripts())
        self.set_version(self.find_version())

    def find_classifiers(self):
        """find_classifiers"""
        return self.config_file.get('setup', 'classifiers', fallback=u"""""")

    def write(self):
        """write"""
        setup_options_formatted = ''
        count = 0

        self.config_file['metadata'].update(version=self.version)
        # all options in setup_options are push_release items
        for item, value in self.config_file['metadata'].items():

            count += 1
            setup_options_formatted += '\n    %s=%s' % (
                item,
                pformat(value, initial_level=2)
            )
            if not count == len(self.config_file['metadata'].keys()):
                setup_options_formatted += ','

        setup_options_formatted += ',\n    %s=%s' % (
            'packages',
            pformat(self.packages, initial_level=2)
        )
        setup_options_formatted += ',\n    %s=%s' % (
            'data_files',
            pformat(self.data_files, initial_level=2)
        )
        setup_options_formatted += ',\n    %s=%s' % (
            'scripts',
            pformat(self.scripts, initial_level=2)
        )

        setup_options_formatted += ',\n'
        setup_options_formatted += '\n'.join([
            '    entry_points={',
            "        console_scripts=%s" % pformat(
                self.console_scripts,
                initial_level=3
            ),
            '    }',
        ])

        setup_options_formatted += '\n'
        setup_options_formatted = setup_options_formatted.replace(
            '"True"', "True"
        )
        setup_options_formatted = setup_options_formatted.replace(
            '"False"', "False"
        )

        file_description = self.config_file.get(
            'setup', 'file_description', fallback=''
        )
        package = self.config_file.get('metadata', 'name')
        if isinstance(self.classifiers, str):
            classifiers = '\n'.join([
                '    ' + x for x in self.classifiers.split('\n')
            ])
        else:
            classifiers = self.classifiers

        setup_code = setup_py_template % dict(
            package=package,
            file_description=file_description,
            generation_time=datetime.datetime.now(),
            setup_options_formatted=setup_options_formatted,
            classifiers=classifiers
        )
        with open(self.path, 'w', encoding='utf-') as setup_file:
            setup_file.write(setup_code)
