# -*- coding: utf-8 -*-
import re


class ChangesProcessor(object):
    """Process changes made from last release"""

    def __init__(self, version_re, commit_re, default_change='Refacoring'):
        """Init changes processor

        :param version_re: regular expression to detect version commit message
        :param commit_re: regular expression to filter commit messages
                          suitable for changes list
        :param default_change: default commit message, used if nothing set

        """
        self.version_re = re.compile(version_re)
        self.commit_re = re.compile(commit_re)

        # default commit message if empty description set in commit
        self._default_commit_message = default_change

        # collected commit messages made since last version
        self.version_lines = []

    def get_default_commit_message(self):
        """Get default commit message"""
        return self._default_commit_message

    def set_default_commit_message(self, commit_message):
        """Set default commit message"""
        self._default_commit_message = commit_message

    default_commit_message = property(
        get_default_commit_message,
        set_default_commit_message
    )
