# -*- coding: utf-8 -*-

# noinspection PyProtectedMember,PyBroadException
try:
    from six.moves.configparser import _UNSET
except Exception:
    _UNSET = None


def parse_datafiles_configuration(current_path, config_file):
    """Take data_files configuration
    and prepare data files structure for setup.py"""
    data_files = dict()
    datafiles_sections = config_file.get('data_files', 'path', fallback=[])

    if isinstance(datafiles_sections, str):
        datafiles_sections = [datafiles_sections]

    for section in datafiles_sections:
        section = section.strip()
        if not section:
            continue

        print("Processing datafiles section %s" % section)
        for item, target_path in config_file.items(section):
            print('Processing datafile %s/%s' % (section, item))

            if target_path == _UNSET:
                continue

            if target_path not in data_files:
                data_files[target_path] = []

            data_files[target_path].append('%s/%s' % (section, item))

    return list(data_files.items())
