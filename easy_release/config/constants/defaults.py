# -*- coding: utf-8 -*-

DEFAULT_GIT_VERSION_RE = r'^v\d+\.\d+\.\d+$'
DEFAULT_MASTER_REPO = 'origin'
DEFAULT_PUSH_VERSION = True
DEFAULT_USE_TAGS = True
