# -*- coding: utf-8 -*-
"""Formatting utilities"""


def pformat(start_obj, indent=4, initial_level=0):
    """Make str representation for dicts & lists"""

    def format_current(obj, level):
        """Format current element"""

        if isinstance(obj, str):
            if (
                    obj.startswith('"') and obj.endswith('"')
            ) or (
                    obj.startswith("'") and obj.endswith("'")
            ):
                obj = obj[1:-1]
            return '"%s"' % str(obj)

        elif isinstance(obj, int) or isinstance(obj, float):
            return '%s' % obj

        elif isinstance(obj, bool):
            return obj and 'True' or 'False'

        elif isinstance(obj, tuple):
            r = '('
            for idx, elem in enumerate(obj):
                is_last = idx == len(obj) - 1
                r += '\n' + ' ' * (level - 0) * indent
                r += format_current(elem, level + 1)
                if not is_last:
                    r += ', '
            r += '\n' + ' ' * (level - 1) * indent + ')'
            return r

        elif isinstance(obj, list):
            r = '[\n'
            for idx, elem in enumerate(obj):
                elem = obj[idx]
                is_last = idx == len(obj) - 1
                r += ' ' * level * indent
                r += format_current(elem, level + 1)
                if not is_last:
                    r += ','
                r += '\n'
            r += ' ' * (level - 1) * indent
            r += ']'
            return r

        elif isinstance(obj, dict):
            keys = [x for x in obj.keys()]
            r = '{\n'
            for idx, key in enumerate(keys):
                key = keys[idx]
                value = obj[key]
                is_last = idx == len(keys) - 1
                r += ' ' * level * indent
                r += '%s=' % key
                r += format_current(value, level + 1)
                if not is_last:
                    r += ','
                r += '\n'
            r += ' ' * (level - 1) * indent
            r += '}'
            return r
        else:
            raise RuntimeError(
                'Unsupported object to format %s %s ' % (type(obj), obj)
            )

    return format_current(start_obj, initial_level)
