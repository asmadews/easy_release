# -*- coding: utf-8 -*-

SECTION_CHANGES = 'changes'
CHANGES_USE_GIT = 'use_git'
CHANGES_GIT_VERSION_RE = 'git_version_regexp'
CHANGES_MASTER_REPO = 'git_master_repo'
CHANGES_PUSH_VERSION = 'git_push_version'
CHANGES_USE_TAGS = 'git_use_tags'
