# -*- coding: utf-8 -*-
"""Utilities to deal with scripts"""


def get_scripts(current_path, config_file):
    """Take data_files configuration
    and prepare data files structure for setup.py"""
    print("Processing scripts section")
    return config_file.get('setup', 'scripts', fallback=[])
