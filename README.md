# Easy Release

The project goal is to give Python project mantainer easy way 
to publish new version of python project.

Usially new version publishing including some simple steps:

- Collect change descritpions into CHANGES.txt
- Increase version number
- Remember current state for easy release next version (at least register 
  which changes was fixed now and dont include into next version changes list)
- Save everything together
- publish changes to end-users 

Because of different project life-circles in different development commands
the system should be able to change it's behaviour basing on configuration.

First step we are making simple tool to deal with simple git projects using git:

- all changes registered in git log, using commit messages as changelog
- version commit detected by special format of commit message or by tag
- configuration file contains everything you need to make setup.py 
- git behaviour managed by config file too

https://setuptools.readthedocs.io/en/latest/setuptools.html#configuring-setup-using-setup-cfg-files
