# -*- coding: utf-8 -*-
"""Define git methods"""

import logging
import os
import re
import warnings
from encodings.utf_8 import decode
from subprocess import PIPE
from subprocess import Popen
from subprocess import STDOUT

from six.moves.configparser import ConfigParser

from easy_release.changes.constants import DEFAULT_CHANGE_MESSAGE
from easy_release.config.constants.defaults import DEFAULT_GIT_VERSION_RE
from easy_release.config.constants.defaults import DEFAULT_MASTER_REPO
from easy_release.config.constants.defaults import DEFAULT_PUSH_VERSION
from easy_release.config.constants.defaults import DEFAULT_USE_TAGS
from easy_release.config.constants.options import CHANGES_GIT_VERSION_RE
from easy_release.config.constants.options import CHANGES_MASTER_REPO
from easy_release.config.constants.options import CHANGES_PUSH_VERSION
from easy_release.config.constants.options import CHANGES_USE_GIT
from easy_release.config.constants.options import CHANGES_USE_TAGS
from easy_release.config.constants.options import SECTION_CHANGES
from easy_release.history_formatter import format_change_line
from easy_release.history_formatter import format_version_line
from easy_release.warnings.deprecation import WARNING_USING_RELATIVE_PATH

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

default_git = 'git'
default_max_versions = 10


class GitDisabled(Exception):
    """Simple exception to detect when git usage disabled"""
    pass


class GitHistoryProcessor(object):
    """Wrap git history log to take changes and commit new releases
    using configured format for commit messages
    """

    def __init__(self, config_file):
        assert isinstance(config_file, ConfigParser)
        self.__config = config_file

        self.version_line_formatter = format_version_line
        self.change_line_formatter = format_change_line
        self.version_lines = []

        self.last_line_is_version = False
        self.last_line_is_change = False

        self.__git_path = None
        self.__version_re = None
        self.__changes_re = None
        self.__master_repo = None
        self.__push_version = None
        self.__use_tags = None

    def get_git_path(self):
        """Get git path

        If default path not suitable it can be set via
        configuration value, f.e.:

        [changes]
        use_git = /usr/local/bin/git

        If using simple use_git = yes or parameter missed,
        it uses path search via :env:`PATH`
        """
        # lazy initialization from config
        if self.__git_path is None:
            self.git_path = self.__config.get(
                SECTION_CHANGES,
                CHANGES_USE_GIT,
                fallback=default_git,
            )
        return self.__git_path

    def set_git_path(self, git_path):
        """Set git path

        :param git_path: </path/to/git>|yes|no
        :type git_path: str

        - yes uses default path %(default_path)s
        - no raises error
        - <path> will checked by os.path.exists and usable if found
        """
        if str(git_path) == 'no':
            raise GitDisabled(
                'Cant use %(class_name)s as %(variable_name)s=no' % dict(
                    class_name=self.__class__.__name__,
                    variable_name=CHANGES_USE_GIT
                )
            )
        if str(git_path).lower() == 'yes':
            git_path = default_git
        else:
            if not os.path.exists(git_path):
                raise GitDisabled(
                    'Git path %(variable_name)s=%(path)s not exists' % dict(
                        path=git_path,
                        variable_name=CHANGES_USE_GIT
                    )
                )

            if not os.path.isabs(git_path):
                warnings.warn(
                    "%(warning)s: Using relative path "
                    "%(variable_name)s=%(path)s "
                    "could forbidden in future" % dict(
                        path=git_path,
                        variable_name=CHANGES_USE_GIT,
                        warning=WARNING_USING_RELATIVE_PATH
                    ),
                    category=PendingDeprecationWarning,
                )
        self.__git_path = git_path

    git_path = property(
        fget=get_git_path,
        fset=set_git_path,
        doc="Configured git path"
    )

    @property
    def usable(self):
        """Check if git usable"""
        try:
            path = self.git_path
            logger.debug("git usable as %s", path)
        except GitDisabled:
            return False
        except Exception as exc:
            raise RuntimeError(
                "Unexpected error while test git usable: %s" % exc
            )
        return True

    def get_version_re(self):
        """Get regular expression to detect commit message as version"""
        if self.__version_re is None:
            self.version_re = self.__config.get(
                SECTION_CHANGES,
                CHANGES_GIT_VERSION_RE,
                fallback=DEFAULT_GIT_VERSION_RE,
            )

        return self.__version_re

    def set_version_re(self, new_version_regex):
        """Set regular expression to match version commit message"""
        self.__version_re = re.compile(new_version_regex)

    version_re = property(
        fget=get_version_re,
        fset=set_version_re,
        doc="Configured regular expression to detect commit message as version"
    )

    def get_changes_re(self):
        """Get regular expression to detect commit message as version"""
        if self.__changes_re is None:
            self.changes_re = self.__config.get(
                SECTION_CHANGES,
                CHANGES_GIT_VERSION_RE,
                fallback=DEFAULT_GIT_VERSION_RE,
            )

        return self.__changes_re

    def set_changes_re(self, new_version_regex):
        """Set regular expression to match version commit message"""
        self.__version_re = re.compile(new_version_regex)

    changes_re = property(
        fget=get_changes_re,
        fset=set_changes_re,
        doc="Configured regular expression "
            "to detect commit message as suitable to write to changes"
    )

    def get_master_repo(self):
        """Get repository name to use in version commit"""
        if self.__master_repo is None:
            self.changes_re = self.__config.get(
                SECTION_CHANGES,
                CHANGES_MASTER_REPO,
                fallback=DEFAULT_MASTER_REPO,
            )

        return self.__changes_re

    def set_master_repo(self, master_repository_name):
        """Set git remote name as master repository to use in version commit"""
        assert isinstance(master_repository_name, str)
        self.__master_repo = master_repository_name

    master_repo = property(
        fget=get_master_repo,
        fset=set_master_repo,
        doc="Configured git remote name to commit and push version commit"
    )

    def get_push_version(self):
        """Get repository name to use in version commit"""
        if self.__push_version is None:
            self.push_version = self.__config.get(
                SECTION_CHANGES,
                CHANGES_PUSH_VERSION,
                fallback=DEFAULT_PUSH_VERSION,
            )

        return self.__push_version

    def set_push_version(self, push_version):
        """Set git remote name as master repository to use in version commit"""
        assert isinstance(push_version, (str, bool))
        if isinstance(push_version, str):
            if push_version.lower() not in ['yes', 'no']:
                raise RuntimeError(
                    "Unexpected parameter %(param)s=%(value)s, "
                    "use yes|no instead" % dict(
                        param=CHANGES_PUSH_VERSION,
                        value=push_version
                    )
                )
            push_version = push_version.lower() == 'yes'
        self.__push_version = push_version

    push_version = property(
        fget=get_push_version,
        fset=set_push_version,
        doc="Push version commit to master repo automatically"
    )

    def get_use_tags(self):
        """Is using tag on version commit"""
        if self.__use_tags is None:
            self.use_tags = self.__config.get(
                SECTION_CHANGES,
                CHANGES_USE_TAGS,
                fallback=DEFAULT_USE_TAGS,
            )

        return self.__push_version

    def set_use_tags(self, use_tags):
        """Set using tag on version commit"""
        assert isinstance(use_tags, (str, bool))
        if isinstance(use_tags, str):
            if use_tags.lower() not in ['yes', 'no']:
                raise RuntimeError(
                    "Unexpected parameter %(param)s=%(value)s, "
                    "use yes|no instead" % dict(
                        param=CHANGES_USE_TAGS,
                        value=use_tags
                    )
                )
            use_tags = use_tags.lower() == 'yes'
        self.__push_version = use_tags

    use_tags = property(
        fget=get_use_tags,
        fset=set_use_tags,
        doc="Push version commit to master repo automatically"
    )

    def add_version_line(self, line):
        """Add new line to changes.

        If line matches version_line pattern and
        previous line was version line too, inserts default change line before
        to have at least one change between versions

        :param line: message
        :type line: str
        """

        default_change = self.__config.get(
            'changes',
            'default_change',
            fallback=DEFAULT_CHANGE_MESSAGE
        )
        if self.last_line_is_version:
            self.add_change_line(default_change)

        self.version_lines.append(self.format_version_line(line))
        self.last_line_is_version = True
        self.last_line_is_change = False

    def add_change_line(self, line):
        """Register message as single change in list of changes

        :param line: message
        :type line: str
        """
        self.version_lines.append(self.format_change_line(line))
        self.last_line_is_change = True
        self.last_line_is_version = False

    def _do_git(self, *git_arguments):
        """Execute git command with specified arguments.

        :returns Popen, int, err
        """
        git_command = [self.git_path]
        git_command.extend(git_arguments)
        logger.info("Executing %s", ' '.join(str(x) for x in git_command))
        p = Popen(
            git_command,
            stdin=PIPE,
            stdout=PIPE,
            stderr=STDOUT,
            close_fds=True
        )
        logger.debug('Subprocess initiated, attach pipes to communicate')
        ret, err = p.communicate()

        if not p.returncode == 0:
            logger.error(
                "Error executing %s", ' '.join(str(x) for x in git_command)
            )
            raise RuntimeError(
                "Error while executing git: %s ERR: %s" % (
                    ret, err
                )
            )
        logger.debug('Finished ok, return result to caller')
        return p, ret, err

    def get_changes_from_git_history(self, max_versions=default_max_versions):
        """Get changes messages from git log over last max_versions

        :param max_versions: maximum versions count to gather messages
        :type max_versions: int
        """
        # TODO: make max_versions configurable

        assert isinstance(max_versions, int)
        self.version_lines = []

        p, ret, err = self._do_git(
            'log',
            '--pretty=oneline',
            '--max-count=200',
            '--simplify-merges'
        )

        if not p.returncode == 0:
            raise RuntimeError(
                "Could not retrieve git log: code %(code)s: %(err)s" % dict(
                    code=p.returncode,
                    err=err
                ))

        change_lines = [
            line[41:].strip()
            for line in ret.decode('utf-8').split('\n')
        ]

        previous_line = ''
        version_count = 0

        for line in change_lines:

            if version_count > max_versions:
                break
            line = line.strip()

            if not line:
                continue

            if previous_line == line:
                continue

            previous_line = line

            if self.version_re.match(line):
                self.add_version_line(line)
                version_count += 1

            if self.changes_re.match(line):
                self.add_change_line(line)

        return self.version_lines

    def get_local_changes(self):
        """Get list of locally changed files using git status"""
        logger.info("Looking for changed files")

        logger.debug("Ask git for changes")
        p, ret, err = self._do_git('status', '--porcelain', '-uno')

        logger.debug("got git response, parse")
        ret = decode(ret)[0]
        new_changes = ret.split('\n')

        logger.debug("Got %s change(s)", len(new_changes))
        return new_changes

    def add_file_into_commit(self, filepath):
        """Add file into next commit"""
        logger.info("Add %s to commit", filepath)
        p1, ret1, err1 = self._do_git('add', filepath)
        if not p1.returncode == 0:
            raise RuntimeError(
                "Error while taking git status: %s ERR: %s" % (
                    ret1, err1
                )
            )

    def commit(self, message):
        """Make commit with requested message"""
        return self._do_git('commit', '-m', message)

    def make_annotated_tag(self, tag, message):
        """Set annotated tag to HEAD"""
        return self._do_git('tag', '-a', tag, '-m', '%s' % message)

    def push(self, *push_args):
        """Do git push with (possible) additional arguments"""
        cmd = ['push']
        cmd.extend(push_args)
        return self._do_git(*cmd)

    def format_version_line(self, line):
        """Format version line to match version_re"""
        # TODO: version_formatter class for both version_re & version_format
        return '%s' % line

    def format_change_line(self, line):
        """Format change line to match change_re"""
        # TODO: change_formatter class for change_re & change_format together
        return self.change_line_formatter(line)

    def commit_version(self, new_version):
        """Make revision commit"""
        version_string = self.format_version_line(new_version)
        logging.info("Commit revision %s", version_string)

        logging.debug("Taking changes made during version generation")
        new_changes = self.get_local_changes()
        for line in new_changes:
            filepath = line.strip().split(' ')[-1]
            logging.debug("Prepare %s to commit", filepath)
            self.add_file_into_commit('./%s' % filepath)

        logging.debug("Commit changed files")
        self.commit(version_string)

        if self.use_tags:
            logging.debug("Attaching annotated tag")
            self.make_annotated_tag(
                version_string,
                "Version %s" % version_string
            )

        if not self.push_version:
            return

        logging.debug("Prepare to push into %s", self.master_repo)

        push_args = [self.master_repo]
        if self.use_tags:
            # push version with version tag
            push_args.append(version_string)

        self.push(*push_args)
