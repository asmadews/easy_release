# -*- coding: utf-8 -*-


def format_change_line(line):
    return '  - ' + line


def format_version_line(line):
    return '- ' + line
